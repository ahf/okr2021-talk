# OKR 2021 Slides

## Building the slides

1. Update the submodules to fetch the `onion-tex` dependency: `git submodule
   update --init --recursive`.

2. Run `make`.
