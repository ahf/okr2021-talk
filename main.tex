%% Copyright (c) 2021 The Tor Project, inc. All rights reserved.
%% Use of this source code is governed by a BSD-style
%% license that can be found in the LICENSE file.
\documentclass[aspectratio=169,12pt]{beamer}

\usepackage[utf8]{inputenc}

\usetheme{onion}

\usepackage[shortlabels]{enumitem}

\setlist[description]{align=parleft, labelwidth=3cm}
\setlist[enumerate]{1., font=\color{OnionDarkPurple}}
\setlist[itemize]{label=$\bullet$, font=\color{OnionDarkPurple}}
\setlist[itemize,2]{label=$\circ$, font=\color{OnionDarkPurple}}

\usepackage{csvsimple}
\usepackage[scale=2]{ccicons}
\usepackage{pgfplots}
\usepackage{xcolor}
\usepackage{booktabs}
\usepackage{fontawesome}


%% We do not set footer number since we async needs to record the slides and we might want to reorder them later.
%% \setbeamertemplate{footline}{\raisebox{5pt}{\makebox[\paperwidth]{\hfill\makebox[20pt]{\color{gray}\scriptsize\insertframenumber}}}\hspace*{5pt}}

\newcommand{\highlight}[1]{%
    \textbf{\alert{#1}}
}

\newcommand{\colorhref}[3][OnionBlack]{%
    \href{#2}{\color{#1}{#3}}
}

\title{Network Team OKR 2021}
\date{September 15, 2021}

\author{\colorhref{mailto:ahf@torproject.org}{Alexander Færøy}}

\institute{}

\titlegraphic{\onionlogocolor}

\setlength{\parskip}{3mm}

\AtBeginPart{
    \let\insertpartnumber\relax
    \let\partname\relax
    \frame{\partpage}
}

\AtBeginSection{
    \let\insertsectionnumber\relax
    \let\sectionname\relax
    \frame{\sectionpage}
}

\AtBeginSubsection{
    \let\insertsubsectionnumber\relax
    \let\subsectionname\relax
    \frame{\subsectionpage}
}

\begin{document}

%% Title Page.
\begin{frame}[plain,noframenumbering]
    \titlepage
\end{frame}

\begin{frame}
    \frametitle{The Network Team}

    Historically, we were the team responsible for everything that touches the
    network. Since the arrival of the Anti-Censorship Team and the Network
    Health Team, this is no longer entirely true.

    We develop and maintain the software to keep the network going:

    \begin{itemize}
        \item Maintaining Tor and Arti.
        \item Build support tools needed to maintain the network:
            experimentation, simulation, testing, etc.
        \item Support other teams and the greater Tor community on Tor issues.
    \end{itemize}

\end{frame}

\begin{frame}
    \frametitle{The Network Team}

    An ordinary day in the Network Team life, we usually see ourselves as:

    \begin{itemize}
        \item Alexander Færøy (Team Lead, Engineering, Anti-Censorship Liaison)
        \item David Goulet (Engineering, Network Health Liaison)
        \item Gaba (Project Management)
        \item Jim Newsome (Engineering, Shadow)
        \item Mike Perry (Network Product Owner, Janitorial Engineering, Raccoon Whisperer)
        \item Nick Mathewson (Software Architecture and Planning, Engineering)
        \item Upcoming Rust Developer \#1 (Engineering)
        \item Upcoming Rust Developer \#2 (Engineering)
    \end{itemize}
\end{frame}

\begin{frame}[t]
    \frametitle{Alex's Role}

    \begin{itemize}
        \item Run 1:1's and our weekly team meetings.

        \item Work with different stakeholders in the organisation.

        \item Coordinate with Gaba, Nick, and Mike on team deliverables.

        \item Engineering tasks.
    \end{itemize}
\end{frame}

\begin{frame}[t]
    \frametitle{\textbf{Objective:} Build and Release Arti}

    \begin{itemize}
        \item[KR] Arti 0.0.1 is released, and has no missing features we deem
            essential for security and privacy.

        \item[KR] Arti 0.1.0 is released, in a form suitable for embedding,
            with measurable client performance.

        \item[KR] All features originally planned for 0.1.0 and 0.0.1 are
            built.

        \item[KR] Arti is less than 4 MB to download.

        \item[KR] Arti's client performance is no more than 30\% slower than C
            Tor for onionperf metrics

        \item[KR] The \texttt{tor-client} API is stable in a way that we're willing to
            support it going forward.
    \end{itemize}
\end{frame}

\begin{frame}[t]
    \frametitle{\textbf{Objective:} Build and Release Arti}

    \begin{itemize}
        \item[KR] Test coverage is above 80\% for all crates.

        \item[KR] Integration tests are complete, automated, and run at least
            daily.

        \item[KR] We have regular reproducible builds for all target platforms
            (Linux, Windows, macOS).

        \item[KR] We have no "Technical Debt" tickets older than 1 month.
    \end{itemize}
\end{frame}

\begin{frame}[t]
    \frametitle{\textbf{Objective:} Build and Release Arti}

    \begin{itemize}
        \item[KR] We have at least 10 active volunteers submitting patches to
            Arti each month.

        \item[KR] At least 4 other projects (that we don't develop) have
            embedded Arti at some level and given us feedback.

        \item[KR] We have at least 10 bug reports or feature requests from
            downstream users each month.

        \item[KR] \url{https://arti.torproject.org} is up.
    \end{itemize}
\end{frame}

\begin{frame}[t]
    \frametitle{\textbf{Objective:} Enhance the Network Performance for Tor users}

    \begin{itemize}
        \item[KR] Develop a Shadow-based workflow suitable for evaluating and
            tuning congestion control changes.

        \item[KR] Have N\% of Exits upgrade to congestion control release 1
            month after stable release. Mike and Alex working on defining N.

        \item[KR] Average throughput increases proportionally to spare
            utilization.

        \item[KR] TTFB/Circuit RTT stays the same or improved.
    \end{itemize}
\end{frame}

\begin{frame}[t]
    \frametitle{\textbf{Objective:} Define how Tor is going to use Tokens}

    These tasks are on our side more related to supporting the goals with
    technical expertise.

    \begin{itemize}
        \item[KR] We have a written specification for a token-system that is
            compatible with Tor's use-cases.

        \item[KR] Any open IETF WG drafts about privacy tokens for the web are
            compatible with the above use-cases, or we are in communication
            with the WG's to try to make them compatible.
    \end{itemize}
\end{frame}

\begin{frame}[t]
    \frametitle{\textbf{Objective:} Support Other Teams with Tor issues}

    \begin{itemize}
        \item[KR] We have no open tickets from these other teams
            (Anti-Censorship, Browser, Network Health) that are marked as
            "blocking" that are older than 1 month.

        \item[KR] We have a process in place to regularly triage the tickets
            listed above, with other teams, and coordinate.
    \end{itemize}
\end{frame}

\begin{frame}[t]
    \frametitle{\textbf{Objective:} Continue to Organize the Team}

    \begin{itemize}
        \item[KR] We have a wiki-page of all responsibilities with links to
            playbook-style documentation.

        \item[KR] For everything that only one person knows how to do, we have
            a playbook for it.

        \item[KR] All network-team documentation is indexed in one place.

        \item[KR] The entire team (anonymously) lists their work related
            happiness as at least 7/10.
    \end{itemize}
\end{frame}

\begin{frame}[t]
    \frametitle{\textbf{Objective:} Continue to Organize the Team}

    \begin{itemize}
        \item[KR] We have automated at least 3 tasks that (as of September 1)
            were done manually.

        \item[KR] Nick has dropped at least one more responsibility.

        \item[KR] We drop at least one task or process as unnecessary.

        \item[KR] We have a process in place to respond to the case where we
            discover a "dropped" or "unhandled" task that nobody was doing, and
            we are tracking how often that happens.
    \end{itemize}
\end{frame}

\begin{frame}[t]
    \frametitle{\textbf{Objective:} The team is spending time in Rust}

    \begin{itemize}
        \item[KR] Every network team programmer has written at least 1000 lines
            that have been merged into Arti or some other Rust project.

        \item[KR] Every network team programmer has reviewed at least 10 MRs
            for Arti or some other Rust project.
    \end{itemize}
\end{frame}

\begin{frame}[t]
    \frametitle{\textbf{Objective:} Continue Shadow Development}

    \begin{itemize}
        \item[KR] Have design and prototype ready for distributed Shadow
            simulations

        \item[KR] Release Shadow 2.0

        \item[KR] Develop a Shadow-based workflow suitable for evaluating and
            tuning congestion control changes (in collab with s61)

        \item[KR] Have Shadow network integration test running with Arti
            clients and C-Tor relays.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Future Work}

    The Network Team will be spending the next period of time on Arti
    development. We believe this is the future of Tor.

    At the same time, we need to continue to support C Tor itself for a while
    for different groups while C Tor is being phased out.

    We will continue to do support for other teams when we are needed for both
    C Tor and Arti.

    We want to continue to organize our team.
\end{frame}

%% Questions page at the end.
\begin{frame}[c, plain, noframenumbering]{}
    \centering

    \vfill

    \huge Questions?

    \vfill

    \normalsize

    \begin{columns}
        \begin{column}{0.60\textwidth}
            \begin{itemize}
                \setlength\itemsep{0.75em}

                \item[\faTwitter] \colorhref{https://twitter.com/ahfaeroey}{@ahfaeroey}
                \item[\faEnvelope] \colorhref{mailto:ahf@torproject.org}{ahf@torproject.org}
                \item[\faKey] OpenPGP: \\ \texttt{1C1B C007 A9F6 07AA 8152} \\ \texttt{C040 BEA7 B180 B149 1921}
            \end{itemize}
        \end{column}

        \begin{column}{0.40\textwidth}
            \begin{center}
                \includegraphics[width=0.95\textwidth]{images/tor_bike.png}
            \end{center}
        \end{column}
    \end{columns}
\end{frame}

%% License slide at the very end.
\begin{frame}[c, plain, noframenumbering]{}
    \centering

    This work is licensed under a

    \large \href{https://creativecommons.org/licenses/by-sa/4.0/}{Creative
    Commons \\ Attribution-ShareAlike 4.0 International License}

    \vfill
    \ccbysa
    \vfill
\end{frame}

\end{document}
