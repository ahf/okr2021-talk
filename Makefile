TARGET     = main.pdf
SOURCE     = main.tex
SOURCES    = $(wildcard *.tex)

export TEXINPUTS:=$(shell pwd)/onion-tex/src/tex:${TEXINPUTS}

all: $(TARGET)

$(TARGET): $(SOURCES)
	latexmk -g -pdf -pdflatex="pdflatex --shell-escape %O %S" $(SOURCE)

clean:
	rm -fr $(TARGET_TEX) tikz/ *.vrb *.dvi *.pdf *.aux *.auxlock *.fdb_latexmk *.fls *.log *.nav *.out *.snm *.toc || true

.PHONY: clean
